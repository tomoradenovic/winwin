package com.gitlab.tomoradenovic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertEquals;

public class LoginTest {

    private ChromeDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();

        // We get the value of "my_headless" system property.
        // In order to run in headless mode, run maven command as follows:
        // mvn -Dmy_headless=true test
        String headless = System.getProperty("my_headless", "false");

        if (headless.equals("true")) {
            chromeOptions.addArguments("--headless");
            // --start-maximized is not a valid option when --headless is used, so we use --window-size instead
            chromeOptions.addArguments("--window-size=1920x1080");
        }
        chromeOptions.addArguments("--start-maximized");

        driver = new ChromeDriver(chromeOptions);

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    @Test
    public void testLoginValidCredentials() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login();

        String url = driver.getCurrentUrl();
        assertEquals("https://www.winwin.rs/customer/account/", url);

        WebElement welcomeTextElement = driver.findElement(By.cssSelector("#mm-0 .hello strong"));
        String welcomeText = welcomeTextElement.getText();
        assertEquals("Dobrodošli, WinWin Test!", welcomeText);
    }

    @Test
    public void addToCart() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.login();

        driver.navigate().to("https://www.winwin.rs/");

        Actions action = new Actions(driver);
        WebElement dropDown = driver.findElement(By.cssSelector("#mm-0 .link-top"));
        int DELAY_TO_SHOW_MENU = 200;
        action
                .moveToElement(dropDown)
                .pause(Duration.ofMillis(DELAY_TO_SHOW_MENU))
                .moveToElement(driver.findElement(By.cssSelector("#nav > ol > li.level0.nav-2.parent > a > span:nth-child(2)")))
                .pause(Duration.ofMillis(DELAY_TO_SHOW_MENU))
                .moveToElement(driver.findElement(By.cssSelector("#nav > ol > li.level0.nav-2.parent > ul > li.level1.nav-2-1.first > a > span")))
                .click()
                .build()
                .perform();

        WebElement productPage = driver.findElement(By.cssSelector("#mm-0 div.category-products > ul > li:nth-child(1) > div.actions > a"));
        productPage.click();

        WebElement increment = driver.findElement(By.cssSelector("#add-to-box > div.add-to-cart > div > div > i.increment-product-qty.i.i-add"));
        increment.click();

        WebElement addToCart = driver.findElement(By.cssSelector("#product-addtocart-button > span:nth-child(2)"));
        addToCart.click();

        String cartUrl = driver.getCurrentUrl();
        assertEquals("https://www.winwin.rs/checkout/cart/", cartUrl);

        WebElement quantity = driver.findElement(By.cssSelector("#shopping-cart-table > tbody > tr > td.a-center.product-cart-qty > input"));
        String quantityText = quantity.getAttribute("value");
        int quantityValue = Integer.parseInt(quantityText);
        assertEquals(2, quantityValue);

        WebElement emptyCart = driver.findElement(By.cssSelector("#shopping-cart-table > tbody > tr > td.a-center.product-cart-actions.last > a:nth-child(3) > span"));
        emptyCart.click();

        WebElement emptyCartMessage = driver.findElement(By.cssSelector("#mm-0 > div > div.main-container.col1-layout > div > div > div.col-main > div.page-title.cart-empty-title > h1"));
        String empptyCartText = emptyCartMessage.getText();
        assertEquals("Korpa je prazna", empptyCartText);
    }

    @Test
    public void logout() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login();

        driver.navigate().to("https://www.winwin.rs/");
        WebElement profileButton = driver.findElement(By.cssSelector("#mm-0 > div > div.header-container > div.header > div.quick-access > div.quick-access-right > ul > li.action-button.account-button > span"));

        Actions action = new Actions(driver);
        int DELAY_TO_SHOW_MENU = 200;
        action
                .moveToElement(profileButton)
                .pause(Duration.ofMillis(DELAY_TO_SHOW_MENU))
                .moveToElement(driver.findElement(By.cssSelector("#mm-0 > div > div.header-container > div.header > div.quick-access > div.quick-access-right > ul > li.action-button.account-button > ul > li > ul > li.last > a")))
                .click()
                .build()
                .perform();

        // wait until website redirects us from "you have successfully logged out" page to homepage
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10L);
        webDriverWait.until(webDriver -> webDriver.getCurrentUrl().equals("https://www.winwin.rs/"));

        WebElement mojNalog = driver.findElement(By.cssSelector("#mm-0 > div > div.header-container > div.header > div.quick-access > div.quick-access-right > ul > li.action-button.account-button > p"));
        Actions action2 = new Actions(driver);
        action2
                .moveToElement(mojNalog)
                .pause(Duration.ofMillis(DELAY_TO_SHOW_MENU))
                .build()
                .perform();

        WebElement prijaviSe = driver.findElement(By.cssSelector("#mm-0 > div > div.header-container > div.header > div.quick-access > div.quick-access-right > ul > li.action-button.account-button > ul > li > ul > li.first > a"));
        String mojNalogText = prijaviSe.getText();
        assertEquals("Prijavi se", mojNalogText);
    }
}
