package com.gitlab.tomoradenovic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage {

    private ChromeDriver driver;

    public LoginPage(ChromeDriver driver) {
        this.driver = driver;
    }

    public void login() {
        driver.navigate().to("https://www.winwin.rs/customer/account/login/");

        WebElement emailField = driver.findElement(By.id("email"));
        emailField.sendKeys(System.getenv("TEST_USERNAME"));

        WebElement passwordField = driver.findElement(By.id("pass"));
        passwordField.sendKeys(System.getenv("TEST_PASSWORD"));

        WebElement submitButton = driver.findElement(By.id("send2"));
        submitButton.click();
    }
}
